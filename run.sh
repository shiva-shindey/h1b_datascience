#!/usr/bin/env bash
#/bin/bash
#Please increase the java heap size based on the dataset and flush the GC before running this code for huge datasets



rm *.class
scalac ./src/Top10Occupations.scala
scalac ./src/Top10States.scala
scala -J-Xmx4g ./src/Top10Occupations.scala ./input/H1B_FY_2016.csv ./output/top_10_occupations.txt
scala -J-Xmx4g ./src/Top10States.scala ./input/H1B_FY_2016.csv ./output/top_10_states.txt






#Please uncomment below lines for running unit test cases
#scalac -cp scalatest_2.12-3.0.4.jar:scalactic_2.12-3.0.4.jar:scalatest_2.13.0-M2-3.2.0-SNAP10.jar UnitTestCases.scala
#scala -cp scalatest_2.12-3.0.4.jar:scalactic_2.12-3.0.4.jar:scalatest_2.13.0-M2-3.2.0-SNAP10.jar org.scalatest.run UnitTestCases
