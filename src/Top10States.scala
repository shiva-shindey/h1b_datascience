import java.io.{FileWriter, PrintWriter}

import scala.io.Source

case class H1bStatesData(CASE_STATUS: String, WORKSITE_STATE: String)

object Top10States extends App {

  override def main(args: Array[String]): Unit = {

    if (args.length == 0) {
      println("Please provide at least one parameter")
    }
    else {

      val filename = args(0).toString

      def readLinesFromFile(filename: String): List[String] = {
        val bufferedSource = scala.io.Source.fromFile(filename)
        val lines = (for (line <- bufferedSource.getLines()) yield line).toList
        bufferedSource.close
        lines
      }

      println("Reading h1b_input File")
      val linesFromH1bInputFile = readLinesFromFile(filename)


      val firstline = {
        val src = Source.fromFile(filename)
        val line = src.getLines.take(1).toList
        src.close
        line
      }
      val header: List[String] = firstline.flatMap((line => line.split(";"))).toList
      var counter = 0

      def getIndex(key: String, header: List[String]): Int = {
        for (column <- header) {
          counter += 1
          if (column contains key)
            return counter
        }
        return -1
      }


      val key1 = "CASE_STATUS"
      val key2 = "WORKSITE_STATE"
      val index1 = getIndex(key1, header) - 1
      counter = 0
      val index2 = getIndex(key2, header) - 1

      println("Maping the input file columns to right schema")
      val h1bDataTable = linesFromH1bInputFile.map {
        raw_line =>
          val columns = raw_line.split("\\;")
          if(columns.length>index2){H1bStatesData(columns(index1), columns(index2))}
          else
            H1bStatesData("","")
      }

      val certifiedApplicants = h1bDataTable.map(x => (x.CASE_STATUS, x.WORKSITE_STATE)).filter(_._1.trim.length > 1).filter(_._2.trim.length > 1).filter(_._1.matches("CERTIFIED"))

      println("Fetching the total certified States")
      val totalCertifiedApplications = certifiedApplicants.size

      println("Determining the number of applications for a given application type or Worksite States")
      val selectingCertifiedApplications = certifiedApplicants.map(record => record._2).foldLeft(Map.empty[String, Int]) { (c, w) => c + (w -> (c.getOrElse(w, 0) + 1)) }

      println("Calculating the percentage value here")
      val output = selectingCertifiedApplications.map {
        case (x, y) => (x, y, (y.toFloat / totalCertifiedApplications) * 100)
      }

      val sortedOutput = output.toList.sortBy(r => (-r._2, r._1)).map(r => (r._1, r._2.toString, r._3.toString + "%"))

      val top10sortedOutput = sortedOutput.take(10)

      var outputHeader: List[(String, String, String)] = List()

      outputHeader = outputHeader :+ (("TOP_STATES", "NUMBER_CERTIFIED_APPLICATIONS", "PERCENTAGE"))

      //val finalOutput : List[(String, String, String)] = top10sortedOutput:+outputHeader

      writeIntoFile(args(1), top10sortedOutput)

      def writeIntoFile(filePath: String, top10sortedOutput: List[(String, String, String)]): Unit = {
        val out = new PrintWriter(new FileWriter(filePath, true))
        try {
          println("writing into file  ")
          outputHeader.foreach { e => out.println(e.productIterator.mkString(";")) }
          top10sortedOutput.foreach { e => out.println(e.productIterator.mkString(";")) }
        }
        finally {
          out.close()
        }

      }

    }
  }
}

