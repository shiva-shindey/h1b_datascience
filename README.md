# H1b_datascience

## Introduciton

I have been working in data engineering since 2016 on different programming languages like Java, Python, and Scala. I did my masters in computer information sciences and I am currently pursuing a technology management degree. 

My hobbies are writing and playing cricket.

I can code in Java, Python and Scala, since, I am learing Functional Programming (it's fun :)), I tried to finish this coding test in Scala.

I will break the solution into five pieces just for better understanding.

## Input Loading & Schema Mapping

Here for input file reading, I am loading only two columns for better performance. There are like roughly 40 columns, we just need 2 columns, so to me it doesn't make any sense to laod everything.

So, I created a case classes which only loads the two params and maps them to those two params of case class. The challenge here was to get the index of these columns from the header of the input file. 

Initially, I thought to use the Scala reflection API for dynamically loading the case classe with new schema information everytime when you load a new input file (because it's hard to get index). On a side note, I have also developed a solution to generate case classes using scala reflection API, please feel free to check the folder Reflect for generating case classes dynamically. 

But I found a simpler solution, so for now, I am following that. Please check this snippet of code which basically gets the index of the columns from input file header.

val firstline = {
        val src = Source.fromFile(filename)
        val line = src.getLines.take(1).toList
        src.close
        line
      }
      val header: List[String] = firstline.flatMap((line => line.split(";"))).toList
      var counter = 0

      def getIndex(key: String, header: List[String]): Int = {
        for (column <- header) {
          counter += 1
          if (column contains key)
            return counter
        }
        return -1
      }


      val key1 = "CASE_STATUS"
      val key2 = "WORKSITE_STATE"
      val index1 = getIndex(key1, header) - 1
      counter = 0
      val index2 = getIndex(key2, header) - 1


## Filtering

I have filtered values which are less than 1 char length, and also I have filtered all the applications which are not ceritifed.

## Aggregation

Here, I basically grouped all the SOC_NAME and WORKSITE_STATE to generate the count of each type. After generating the group count, I have also generated the percentage for one record.

Please check this code snippet below.

	  println("Fetching the total certified States")
      val totalCertifiedApplications = certifiedApplicants.size

      println("Determining the number of applications for a given application type or Worksite States")
      val selectingCertifiedApplications = certifiedApplicants.map(record => record._2).foldLeft(Map.empty[String, Int]) { (c, w) => c + (w -> (c.getOrElse(w, 0) + 1)) }

      println("Calculating the percentage value here")
      val output = selectingCertifiedApplications.map {
        case (x, y) => (x, y, (y.toFloat / totalCertifiedApplications) * 100)
      }


## Output

In output section, I just sorted in descending first by first by number of certified applicants and then by occupations, similarly, I did the same for states also. 

Adding snippet below:

	  val sortedOutput = output.toList.sortBy(r => (r._2, r._1)).reverse

      val top10sortedOutput = sortedOutput.take(10)

      val top10List = top10sortedOutput.flatMap(t => List(t._1, t._2.toString, t._3.toString))

## Conclusion

Overall, I would like to improve my solution by adding more unit test cases, more meaningfull ones. I also would like to make the code more modular, I tried to make it modular but I know I can improve this If I have time.

I am still thinking on how to efficiently read the data, I always want to implement this in different ways.

There may be some bugs, like I didn't add % symbol in output, I may have not removed the quote's etc. I will work on them remove/add them asap.

[My Github Link](https://github.com/ShivaShinde)

## Edit
I have fixed few bugs, now the output looks good to me. 
Bug 1: The output didn't have a header
Bug 2, It was raising indexOutOfBoundException for some records while loading the input using case classes, I fixed that bug.
Bug 3, I have added % symbol for all the percentages.
Bug 4, Ordering for second column got messed up, now I fixed it.

## Improvements
I need to add unit test cases. I have added the funsuite jar, will add them soon.
I will see If I can read the input once and generate outputs for both STATES and OCCUPATIONS at once rather than having two different classes and files.

**NOTE:** If the run.sh doesn't work, please manually run the file from src, it should work that way. For some weird reason, sh run.sh is compiling fine but not executing properly.
